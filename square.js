export function ColoredSquare({
    color, name
}){
    return(
        <div>
            <div style={{backgroundColor: color, width: 80, height: 80}}>
                
            </div>
            {
                <span>{color}</span>
            }
            {
                <p>{name}</p>
            }
        </div>
        // <div>
        //     {
        //         <p>color</p>
        //     }
        // </div>
    )
}