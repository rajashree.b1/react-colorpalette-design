import { ColoredSquare } from "../square"


// Primary color List
export function ColorPalette(){
    const primaryColorList=[
        {
            name: "Primary 0",
            color: '#6E2665'
        },
        {
            name: "Primary 1",
            color: '#953389'
        },
        {
            name: "Primary 2",
            color: '#B758AB'
        },
        {
            name: "Primary 3",
            color: '#FFD6FA'
        },
        {
            name: "Primary 4",
            color: '#FAECF9'
        },
        {
            name: "Primary 5",
            color: '#FCF8FC'
        }
    ]
    return (
        <div style={{display: "flex", alignItems: "center"}}>
            {/* <p>Rajashree B.</p> */}
            {/* <ColoredSquare color={'#f00000'}></ColoredSquare>
            <ColoredSquare color={'#000000'}></ColoredSquare>
            <ColoredSquare color={'#f77777'}></ColoredSquare>
            <ColoredSquare color={'#cccccc'}></ColoredSquare>
            <ColoredSquare color={'#eeeeee'}></ColoredSquare> */}
            {
                primaryColorList.map((colorData)=>(
                    <ColoredSquare color={colorData.color} name={colorData.name}></ColoredSquare>
                ))
            }
        </div>
    )
}

// Text color List
export function ColorPalette2(){
    const textColorList=[
        {
            name: "Primary 0",
            color: '#6E2665'
        },
        {
            name: "Primary 1",
            color: '#953389'
        },
        {
            name: "Primary 2",
            color: '#B758AB'
        },
        {
            name: "Primary 3",
            color: '#FFD6FA'
        },
        {
            name: "Primary 4",
            color: '#FAECF9'
        },
        {
            name: "Primary 5",
            color: '#FCF8FC'
        }
    ]
    return (
        <div style={{display: "flex", alignItems: "center"}}>
            {
                textColorList.map((colorData)=>(
                    <ColoredSquare color={colorData.color} name={colorData.name}></ColoredSquare>
                ))
            }
        </div>
    )
}

// Secondary color List
